//
//  Weather.swift
//  WeatherApplication
//
//  Created by Vlad Maevsky on 4/29/18.
//  Copyright © 2018 vladm. All rights reserved.
//

import Foundation
import CoreLocation

struct Weather {
    let summary: String
    let icon: String
    let temperature: Double
    let nameCountryCity: String
    
    enum SerializationError: Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    
    init(json: [String : Any], nameCountryCity: String) throws {
        guard let summary = json["summary"] as? String else { throw SerializationError.missing("summary is missing")}
        guard let icon = json["icon"] as? String else { throw SerializationError.missing("icon is missing")}
        guard let temperature = json["temperatureMax"] as? Double else { throw SerializationError.missing("temp is missing")}
        
        self.summary = summary
        self.icon = icon
        self.temperature = temperature
        self.nameCountryCity = nameCountryCity
        
    }
    
    
    static let basePath = "https://api.darksky.net/forecast/a1c1648327e98c7fc52dcb001f7192f8/"
    
    static func forecast (withLocation location:CLLocationCoordinate2D, completion: @escaping ([Weather]?) -> ()) {
        
        let url = basePath + "\(location.latitude),\(location.longitude)"
        let request = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            var forecastArray = [Weather]()
            
            if let data = data {
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        let timeZoneName = json["timezone"] as? String ?? ""
                        
                        if let dailyForecasts = json["daily"] as? [String : Any] {
                            if let dailyData = dailyForecasts["data"] as? [[String : Any]] {
                                for dataPoint in dailyData {
                                    if let weatherObject = try? Weather(json: dataPoint, nameCountryCity: timeZoneName) {
                                        forecastArray.append(weatherObject)
                                    }
                                }
                            }
                        }
                    }
                } catch {
                    print(error.localizedDescription)
                }
                completion(forecastArray)
            }
        }
        task.resume()
    }
}
