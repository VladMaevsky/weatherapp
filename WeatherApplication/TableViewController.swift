//
//  TableViewController.swift
//  WeatherApplication
//
//  Created by Vlad Maevsky on 4/29/18.
//  Copyright © 2018 vladm. All rights reserved.
//

import UIKit
import CoreLocation

class TableViewController: UITableViewController, UISearchBarDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var imageViewMain: UIImageView!
    @IBOutlet weak var labelMain: UILabel!
    @IBOutlet weak var labelMainDescription: UILabel!
    @IBOutlet weak var labelCityName: UILabel!
    
    @IBAction func actionRefresh(_ sender: UIRefreshControl) {
        
        if let searchBarStr = searchBar.text {
            str += searchBarStr
        } else if let currentCoordStr = locationManager.location?.coordinate {
            str += "\(currentCoordStr)"
        } else {
            str = defaultCity
        }

        updateWeatherForLocation(location: str)
    }
    
    var forecastData = [Weather]()
    
    let defaultCity = "Minsk"
    var str = ""
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        searchBar.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print(location.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.denied {
            updateWeatherForLocation(location: defaultCity)
        } else if status == CLAuthorizationStatus.authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            updateWeatherForLocation(location: "\(String(describing: locationManager.location?.coordinate))")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let locationString = searchBar.text, !locationString.isEmpty {
            updateWeatherForLocation(location: locationString)
        }
    }
    
    
    func updateWeatherForLocation (location: String) {
        CLGeocoder().geocodeAddressString(location) { (placemarks: [CLPlacemark]?, error: Error?) in
            if error == nil {
                if let location = placemarks?.first?.location {
                    Weather.forecast(withLocation: location.coordinate, completion: { (results: [Weather]?) in
                        if let weatherData = results {
                            self.forecastData = weatherData
                            
                            DispatchQueue.main.async {
                                self.refreshControl?.endRefreshing()
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return forecastData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let date = Calendar.current.date(byAdding: .day, value: section, to: Date())
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM, dd, yyyy"
        
        return dateFormatter.string(from: date!)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let weatherObject = forecastData[indexPath.section]
        
        cell.textLabel?.text = weatherObject.summary
        cell.detailTextLabel?.text = "\(Int((weatherObject.temperature - 32) * 0.55)) °C"
        cell.imageView?.image = UIImage.init(named: weatherObject.icon) ?? UIImage.init(named: "partly-cloudy-day")
        
        labelCityName.text = "\(forecastData[0].nameCountryCity.split(separator: "/")[1])" //It will show name of timezone, not city. Unfortunetally, this source of weather isn't provide a name of city, timezone only.
        labelMain.text = "\(Int((forecastData[0].temperature - 32) * 0.55)) °C"
        imageViewMain.image = UIImage.init(named: forecastData[0].icon)
        labelMainDescription.text = forecastData[0].summary
        
        return cell
    }
}
